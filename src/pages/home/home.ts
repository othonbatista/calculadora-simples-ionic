import { Component } from '@angular/core';

import { NavController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  primeiroNumero : string;
  segundoNumero : string;

  constructor(
    public navCtrl: NavController,
    public toastCtrl : ToastController
  ) {

  }

  operacaoNumeros (operacao : string) : void {

    var resultado : number = 0;
    var opNome : string = "";
    var opSimbolo : string = "";

    if (operacao == "add") {
      resultado = parseFloat (this.primeiroNumero) +
                              parseFloat (this.segundoNumero);
      opNome = "Soma";
      opSimbolo = "+";
    }

    if (operacao == "sub") {
      resultado = parseFloat (this.primeiroNumero) -
                              parseFloat (this.segundoNumero);
      opNome = "Subtração";
      opSimbolo = "-";
    }

    if (operacao == "mul") {
      resultado = parseFloat (this.primeiroNumero) *
                              parseFloat (this.segundoNumero);
      opNome = "Multiplicação";
      opSimbolo = "x";
    }

    if (operacao == "div") {
      resultado = parseFloat (this.primeiroNumero) /
                              parseFloat (this.segundoNumero);
      opNome = "Divisão";
      opSimbolo = "/";
    }

    let toast = this.toastCtrl.create ({
      message : this.primeiroNumero +
                " " + opSimbolo + " " + this.segundoNumero +
                " = " + resultado,
      showCloseButton : true,
      closeButtonText : "Fecha"
    });

    toast.present ();

  }

}
